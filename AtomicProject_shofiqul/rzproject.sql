-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 12:38 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rzproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `dates` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `dates`) VALUES
(1, 'Rashid', '1994-09-21'),
(2, 'Raju', '1991-09-21'),
(4, 'Shafik', '1992-12-26');

-- --------------------------------------------------------

--
-- Table structure for table `booklist`
--

CREATE TABLE IF NOT EXISTS `booklist` (
`id` int(11) NOT NULL,
  `name` varchar(66) NOT NULL,
  `item` varchar(33) NOT NULL,
  `item2` varchar(33) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booklist`
--

INSERT INTO `booklist` (`id`, `name`, `item`, `item2`) VALUES
(1, 'Rashid', 'HTML Book', 'CSS Book'),
(2, 'Raju', 'HTML Book', 'CSS Book'),
(3, 'Shariar', 'HTML Book', 'CSS Book'),
(9, 'Raju', 'HTML Book', 'CSS Book'),
(11, 'Rana', '', 'CSS Book'),
(12, 'Rashid', '', 'CSS Book');

-- --------------------------------------------------------

--
-- Table structure for table `bookmark`
--

CREATE TABLE IF NOT EXISTS `bookmark` (
`id` int(11) NOT NULL,
  `title` varchar(60) NOT NULL,
  `url` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmark`
--

INSERT INTO `bookmark` (`id`, `title`, `url`) VALUES
(1, 'w3schools', 'http://w3schools.com'),
(2, 'bdjobs', 'http://bdjobs.com'),
(3, 'php', 'http://php.net'),
(4, 'youtube', 'http://youtube.com'),
(5, 'facebook', 'http://facebook.com'),
(6, 'flatuicolors', 'https://flatuicolors.com/'),
(9, 'FortAwesome', 'http://fortawesome.github.io/Font-Awesome/');

-- --------------------------------------------------------

--
-- Table structure for table `countrylist`
--

CREATE TABLE IF NOT EXISTS `countrylist` (
`id` int(11) NOT NULL,
  `name` varchar(66) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `countrylist`
--

INSERT INTO `countrylist` (`id`, `name`, `country`) VALUES
(1, 'Shafik', 'India'),
(2, 'Rashid', 'Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `genderlist`
--

CREATE TABLE IF NOT EXISTS `genderlist` (
`id` int(11) NOT NULL,
  `name` varchar(66) NOT NULL,
  `gender` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genderlist`
--

INSERT INTO `genderlist` (`id`, `name`, `gender`) VALUES
(2, 'Badsha', 'Male'),
(3, 'Shafik', 'Male'),
(4, 'Retu', 'Female'),
(5, 'Zina', 'Female'),
(6, 'Shariar', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `library`
--

CREATE TABLE IF NOT EXISTS `library` (
`id` int(11) NOT NULL,
  `title` varchar(66) NOT NULL,
  `author` varchar(66) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `library`
--

INSERT INTO `library` (`id`, `title`, `author`) VALUES
(1, 'PHP Book', 'Rashid'),
(2, 'CSS Book', 'Rana'),
(5, 'jQuery', 'shafik'),
(6, 'Lass', 'raju');

-- --------------------------------------------------------

--
-- Table structure for table `profilelist`
--

CREATE TABLE IF NOT EXISTS `profilelist` (
`id` int(11) NOT NULL,
  `name` varchar(66) NOT NULL,
  `paths` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilelist`
--

INSERT INTO `profilelist` (`id`, `name`, `paths`) VALUES
(4, 'Rana', 'Uploads/canberra_hero_image.jpg'),
(5, '3613131', 'Uploads/man-08.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
`id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email`) VALUES
(1, 'Rashid', 'rashidrana.r@gmail.com'),
(2, 'Imran', 'mdkhan.imran29@gmail.com'),
(3, 'Arster', 'arster7777@yahoo.com'),
(4, 'Rana', 'rashidran.rashid@gmail.com'),
(5, 'Saiful', 'mdsaiful359@yahoo.com'),
(6, 'Raju', 'raju@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booklist`
--
ALTER TABLE `booklist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmark`
--
ALTER TABLE `bookmark`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countrylist`
--
ALTER TABLE `countrylist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genderlist`
--
ALTER TABLE `genderlist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `library`
--
ALTER TABLE `library`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilelist`
--
ALTER TABLE `profilelist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `booklist`
--
ALTER TABLE `booklist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `bookmark`
--
ALTER TABLE `bookmark`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `countrylist`
--
ALTER TABLE `countrylist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `genderlist`
--
ALTER TABLE `genderlist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `library`
--
ALTER TABLE `library`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profilelist`
--
ALTER TABLE `profilelist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
