<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Birthdays\Birthday;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Birthday(NULL,$_GET);
		$obj->deletes();
	}else{
		Utility::redirect($_SERVER["HTTP_REFERER"]);
	}
?>