<?php
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\BookSelection\BookList;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new BookList(NULL,$_GET);
		$result = $obj->show();
	}else{
		Utility::redirect("index.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(29) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 3;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>Edit Your Book</h1>
					<div class="message"><?= Utility::message(); ?></div>
					<div class="formArea">
					<form class="form-horizontal" method="post" action="update.php?id=<?= $result->id; ?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">Your Name :</label>
							<div class="col-sm-6">
								<input 
								class="form-control"
								type="text" 
								name="uName"
								placeholder="Name"
								value="<?= $result->name; ?>"
								>
							</div>
						</div>
						<div class="form-group marginLeft">
							
							<div class="col-sm-6">
								<input 
								id="boxId1"
								class=""
								type="checkbox" 
								name="item"
								<?php if(!empty($result->item)){ echo "checked";} ; ?>
								value="HTML Book"
								>
								<label for="boxId1">HTML Book :</label>
							</div>
							
						</div>
						<div class="form-group marginLeft">
							<div class="col-sm-6">
								<input 
								id="boxId2"
								class=""
								type="checkbox" 
								name="item2"
								<?php if(!empty($result->item2)){ echo "checked";} ; ?>
								value="CSS Book"
								>
								<label for="boxId2">CSS Book :</label>
							</div>
							
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" name="submit" class="btn btn-success">Save</button>
							</div>
						  </div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by <a href="#">Rashid</a></p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>



