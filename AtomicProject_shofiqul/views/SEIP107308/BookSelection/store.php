<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\BookSelection\BookList;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"])){
		$obj = new BookList($_POST);
		$obj->store();
	}else{Utility::redirect("create.php");}
?>