<?php
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\BookSelection\BookList;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new BookList($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>