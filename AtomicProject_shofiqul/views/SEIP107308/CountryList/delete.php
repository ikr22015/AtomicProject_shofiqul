<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\CountryList\Country;
	use App\Bitm\SEIP107308\Utility\Utility;
	
	if(isset($_GET["id"])){
		$obj = new Country(NULL,$_GET);
		$obj->deletes();
	}else{
		Utility::redirect($_SERVER["HTTP_REFERER"]);
	}
?>