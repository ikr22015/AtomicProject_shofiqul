<?php
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\CountryList\Country;
	use App\Bitm\SEIP107308\Utility\Utility;
	
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new Country($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>