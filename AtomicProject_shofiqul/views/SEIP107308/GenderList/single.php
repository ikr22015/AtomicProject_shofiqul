<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\GenderList\Gender;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Gender(NULL,$_GET);
		$result = $obj->show();
	}else{
		Utility::redirect("index.php");
	}
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(29) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 2;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>Your Name & Gender!!!</h1>
					
					<div class="storeArea">
						<div class="viewArea">
							<div class="view">
								<label>Name :</label>
								<p><?= $result->name; ?></p>
							</div>
						
							<div class="view">
								<label>Your Gender :</label>
								<p><?= $result->gender; ?></p>
							</div>
						</div>
						
						<ul class="viewLink">
							<?php 
								$files = ["index","edit","delete"];
								foreach($files as $key=>$file){
									if($file == "index"){
										$name = "List";
										echo "<li><a href='{$file}.php'>{$name}</a></li>";
									}else{
										$name = ucfirst($file);
										echo "<li><a href='{$file}.php?id={$result->id}'>{$name}</a></li>";
									}
								}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by <a href="#">Rashid</a></p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>