<?php
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\GenderList\Gender;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new Gender($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>