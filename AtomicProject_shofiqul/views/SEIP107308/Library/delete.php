<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Library\Books;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Books(NULL,$_GET);
		$obj->deletes();
	}else{
		Utility::redirect($_SERVER["HTTP_REFERER"]);
	}
?>