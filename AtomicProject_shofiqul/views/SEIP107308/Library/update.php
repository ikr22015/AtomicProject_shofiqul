<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Library\Books;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new Books($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>