<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\ProfileList\Profiles;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"]) && isset($_GET["id"])){
		$obj = new Profiles($_POST,$_GET,$_FILES);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>