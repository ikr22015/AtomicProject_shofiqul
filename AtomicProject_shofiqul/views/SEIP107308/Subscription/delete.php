<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Subscription\Subscribe;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Subscribe(NULL,$_GET);
		$obj->deletes();
	}else{
		Utility::redirect($_SERVER["HTTP_REFERER"]);
	}
?>