<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Tools\Bookmark;
	use App\Bitm\SEIP107308\Utility\Utility;
	$obj = new Bookmark(NULL,$_GET);
	$result = $obj->show();
?>
<!DOCTYPE html>
<html>
<head><meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(29) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 3;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>Edit Your Bookmark</h1>
					<div class="message"><?= Utility::message(); ?></div>
					<div class="formArea">
					<form class="form-horizontal" method="post" action="update.php?id=<?= $result->id; ?>">
						<div class="form-group">
							<label class="col-sm-2 control-label">Title :</label>
							<div class="col-sm-6">
								<input 
								class="form-control"
								type="text"
								name="title"
								placeholder="Title"
								value="<?= $result->title; ?>"
								>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Site URL :</label>
							<div class="col-sm-6">
								<input 
								class="form-control"
								type="url"
								name="url"
								placeholder="URL"
								value="<?= $result->url; ?>"
								>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" class="btn btn-success">Save</button>
							</div>
						  </div>
					</form>
					</div>
				</div>
			</div>
		</div>
		<footer id="footer">
			<div class="footerArea">
				<div class="footer">
					<p>Design & Developed by <a href="#">Rashid</a></p>
				</div>
			</div>		
		</footer>
	</div>
</body>
</html>



