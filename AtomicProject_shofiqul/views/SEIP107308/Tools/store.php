<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Tools\Bookmark;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_POST["submit"])){
		$obj = new Bookmark($_POST);
		$obj->store();
	}else{
		Utility::redirect("create.php");
	}
?>
