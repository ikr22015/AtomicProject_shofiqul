<?php 
	require_once("../../startup.php");
	use App\Bitm\SEIP107308\Tools\Bookmark;
	use App\Bitm\SEIP107308\Utility\Utility;
	if(isset($_GET["id"])){
		$obj = new Bookmark($_POST,$_GET);
		$obj->update();
	}else{
		Utility::redirect("index.php");
	}
?>